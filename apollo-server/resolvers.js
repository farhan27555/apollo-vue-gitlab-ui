import GraphQLJSON from 'graphql-type-json'


export default {
  JSON: GraphQLJSON,


  Query: {
    // Here I added the query to fetch the data from db
    tableData: (root, args, { db }) => db.get('tableData').value(),

  },

  Mutation: {
    // Here I added the mutation to add the data to db
    addDataToTable: (root, { input }, { pubsub,db }) => {
      const tableData = {
        firstName: input.firstName,
        lastName: input.lastName,
      }

      db
        .get('tableData')
        .push(tableData)
        .last()
        .write()
      console.log(tableData)
      pubsub.publish('tableData', { tableDataAdded: tableData })

      return tableData
    },
  },

  Subscription: {
    tableDataAdded: {
      subscribe: (parent, args, { pubsub }) => pubsub.asyncIterator('tableData'),
    },

  },
}
